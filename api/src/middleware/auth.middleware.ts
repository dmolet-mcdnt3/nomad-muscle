import { RequestHandler, Response } from "express";
import jwt from "jsonwebtoken";
import config from "../../config/config"

export interface TokenPayload {
  user: {
    id: string,
    email: string,
    birthdate: string,
    firstname: string,
    lastname: string
  }
}

export const verifyAuth : RequestHandler = function(req, res, next) {
  const authHeader = req.headers.authorization;
  if (!authHeader) {
    return res.status(401).send({ message: "Unauthorized." })
  }

  if (!authHeader.startsWith("Bearer ")) {
    return res.status(400).send({ message: "Invalid authorization header." });
  }

  const token = authHeader.slice(7, authHeader.length);
  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) return res.send(err);
    setTokenPayload(res, decoded as TokenPayload);
    next();
  })
};

export function setTokenPayload(res : Response, payload : TokenPayload) {
  res.locals.authToken = payload;
}

export function getTokenPayload(res : Response) : TokenPayload {
  return res.locals.authToken;
}
