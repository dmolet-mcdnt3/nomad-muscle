import { Router } from "express";
import { verifyAuth } from "../middleware/auth.middleware";
import { createGym, getGym, editGym, getGyms, getMyGyms } from "../controllers/gyms.controller";

const router = Router();

router.route("")
  .get(getGyms)
  .all(verifyAuth)
  .post(createGym);

router.route("/mine")
  .all(verifyAuth)
  .get(getMyGyms);

router.route("/:id")
  .get(getGym)
  .all(verifyAuth)
  .put(editGym);

export default router;
