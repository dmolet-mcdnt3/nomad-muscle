import express from "express";
import userRouter from "./user.router";
import gymRouter from "./gym.router";

export default (app : express.Application) => {
  app.use("/user", userRouter);
  app.use("/gyms", gymRouter);
}
