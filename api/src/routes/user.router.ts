import { Router } from "express";
import * as userController from "../controllers/user.controller";

const router = Router()
  .post("/login", userController.login)
  .post("/register", userController.createUser);

export default router;
