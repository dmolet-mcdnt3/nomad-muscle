import expressValidator from "express-validator/check";

type Fields<T> = Extract<keyof T, string> | Array<Extract<keyof T, string>>;

export function body<T>(fields : Fields<T>) {
  return expressValidator.body(fields);
}

export function query<T>(fields : Fields<T>) {
  return expressValidator.query(fields);
}

export function check<T>(fields : Fields<T>) {
  return expressValidator.check(fields);
}
