import mongoose, { Schema, Types } from "mongoose";
import { ObjectId } from "bson";
import { IMongooseSchema } from "./IMongooseSchema";

export interface Booking {
  start_date: Date;
  end_date: Date;
  guest: ObjectId;
  confirmed: boolean;
}

export interface BookingModel extends Booking, mongoose.Document {}

export const BookingSchema = new Schema(<IMongooseSchema<Booking>>{
  start_date: {
    type: Date,
    required: true
  },
  end_date: {
    type: Date,
    required: true
  },
  guest: {
    type: Types.ObjectId,
    default: null,
    ref: "User"
  },
  gym: {
    type: Types.ObjectId,
    required: true,
    ref: "Gym"
  },
  confirmed: {
    type: Boolean,
    default: false
  }
});

export const bookingModel = mongoose.model<BookingModel>("booking", BookingSchema);
