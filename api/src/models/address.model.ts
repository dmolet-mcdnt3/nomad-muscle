import { Schema } from "mongoose";
import { IMongooseSchema } from "./IMongooseSchema";

export interface Address {
  line1 : string
  line2 : string
  city : string
  postalCode : string,
};

export const AddressSchema = new Schema(<IMongooseSchema<Address>> {
  line1: {
    type: String,
    required: true,
    trim: true
  },
  line2: {
    type: String,
    trim: true
  },
  city: {
    type: String,
    required: true,
    trim: true
  },
  postalCode : {
    type: String,
    required: true,
    trim: true,
    validate: /\d{5}/
  }
});
