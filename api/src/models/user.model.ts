import mongoose, { Schema } from "mongoose";
import { AddressSchema, Address } from "./address.model";
import { IMongooseSchema } from "./IMongooseSchema";

export interface User {
  email: string;
  password?: string;
  firstname: string;
  lastname: string;
  address: Address;
  birthdate: Date;
  balance: number;
  id_doc: string;
  medical_certificate: string;
}

export interface UserModel extends User, mongoose.Document {}

export const UserSchema = new Schema(<IMongooseSchema<User>>{
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
    select: false
  },
  firstname: {
    type: String,
    required: true,
    trim: true
  },
  lastname: {
    type: String,
    required: true,
    trim: true
  },
  address: {
    type: AddressSchema,
    required: true
  },
  birthdate: {
    type: Date,
    required: true
  },
  balance: {
    type: Number,
    required: true,
    default: 0
  },
  id_doc: {
    type: String
  },
  medical_certificate: {
    type: String
  }
});

export const userModel = mongoose.model<UserModel>("User", UserSchema);
