import { SchemaDefinition } from "mongoose";

type SchemaDefinitionProptype = SchemaDefinition[keyof SchemaDefinition];
export type IMongooseSchema<T> = Record<keyof T, SchemaDefinitionProptype>;
