import mongoose from "mongoose";
import { AddressSchema, Address } from "./address.model";
import { IMongooseSchema } from "./IMongooseSchema";

export interface Gym {
  owner: mongoose.Types.ObjectId;
  title: string;
  description: string;
  hourly_rate: number;
  address: Address;
  services: string[];
  equipment: string[];
  size?: { width: number, length: number },
  pictures: string[];
  capacity: number;
  cancellation_fee: number;
  auto_accept: boolean;
  public: boolean;
}

export interface GymModel extends Gym, mongoose.Document {}

export const GymSchema = new mongoose.Schema(<IMongooseSchema<Gym>>{
  owner: {
    type: mongoose.Types.ObjectId,
    required: true,
    index: true,
  },
  title: {
    type: String,
    required: true,
    trim: true
  },
  description: {
    type: String,
    default: "",
    trim: true
  },
  hourly_rate: {
    type: Number,
    required: true
  },
  address: {
    type: AddressSchema,
    required: true
  },
  location: {
    type: { type : String },
    coordinates: [],
    select: false
  },
  services: {
    type: [{
      type: String,
      enum: [
        "shower",
        "parking",
        "drinks"
      ]
    }]
  },
  equipment: {
    type: [ {
      type: String,
      enum: [
        "power-cage",
        "power-tower",
        "heavy-bag",
        "gym-rings",
        "balance-trainer",
        "air-rower",
        "treadmill",
        "stationary-bike"
      ]
    } ],
    required: true
  },
  size: {
    type: { width: Number, length: Number },
    default: null
  },
  pictures: {
    type: [ String ],
    required: true,
    default: []
  },
  capacity: {
    type: Number,
    required: true,
    validate: (val : Number) => val > 0
  },
  cancellation_fee: {
    type: Number,
    default: 0,
    required: true
  },
  auto_accept: {
    type: Boolean,
    default: false,
    required: true
  },
  public : {
    type: Boolean,
    required: true,
    default: false
  }
});

GymSchema.index({ public: 1}, {
  partialFilterExpression: {
    public: true
  }
});

GymSchema.index({ hourly_rate: 1}, {
  partialFilterExpression: {
    public: true
  }
});

GymSchema.index({ location: "2dsphere"}, {
  partialFilterExpression: {
    public: true
  }
});

export const gymModel = mongoose.model<GymModel>("Gym", GymSchema);
