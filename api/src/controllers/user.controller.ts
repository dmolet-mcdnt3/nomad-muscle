import { RequestHandler } from "express";
import { userModel } from "../models/user.model";
import jwt from "jsonwebtoken";
import { sha256 } from "js-sha256";
import config from "../../config/config";
import { Address } from "../models/address.model";
import { TokenPayload } from "../middleware/auth.middleware";
import { catchValidationErrors } from "../middleware/validation.middleware";
import { body } from "../utils/express-validator";
import { MongoError } from "mongodb";

interface createUserBody {
  email: string;
  password: string;
  firstname: string;
  lastname: string;
  birthdate: string;
  address: Address;
}

export const createUser : RequestHandler[] = [
  body<createUserBody>("email").isEmail(),
  body<createUserBody>(["password", "firstname", "lastname"]).not().isEmpty(),
  catchValidationErrors,
  async (req, res) => {
    const body : createUserBody = req.body;
    body.password = sha256(body.password);
    const user = new userModel({
      address: body.address,
      birthdate: body.birthdate,
      email: body.email,
      firstname: body.firstname,
      lastname: body.lastname,
      password: body.password
    });
    try {
      await user.save();
    } catch (err) {
      if (err instanceof MongoError && err.code === 11000) {
        res.status(422).json({ message: "Duplicate email."});
        return;
      }
      throw err;
    }
    res.status(200);
  }
];

interface loginBody {
  email: string;
  password: string;
}

export const login : RequestHandler[] = [
  body<loginBody>("email").isEmail(),
  body<loginBody>("password").not().isEmpty(),
  catchValidationErrors,
  async (req, res) => {
    const body = req.body as loginBody;
    body.password = sha256(body.password);

    const user = await userModel.findOne({
      email: body.email.toLowerCase(),
      password: body.password
    }).exec();

    if (!user) {
      return res.status(403).json({ message: 'Incorrect credentials.' });
    }

    const tokenContent : TokenPayload = {
      user: {
        id: user._id,
        email: user.email,
        birthdate: user.birthdate.toISOString(),
        firstname: user.firstname,
        lastname: user.lastname
      }
    };
    const token = jwt.sign(tokenContent, config.secret, { expiresIn: "170h" });
    res.status(200).json({ data: { token: token }});
  }
];
