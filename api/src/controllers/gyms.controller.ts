import { RequestHandler } from "express";
import { body } from "../utils/express-validator";
import { Address } from "../models/address.model";
import { catchValidationErrors } from "../middleware/validation.middleware";
import { gymModel, Gym } from "../models/gym.model";
import { getTokenPayload } from "../middleware/auth.middleware";
import { ObjectId } from "bson";

interface gymForm {
  title: string;
  hourly_rate: number;
  cancellation_fee: number;
  capacity: number;
  address: Address,
  services: string[];
  equipment: string[];
  size?: { width: number, length: number };
  auto_accept: boolean;
}

export const createGym : RequestHandler[] = [
  body<gymForm>(["title", "address"]).not().isEmpty(),
  body<gymForm>(["hourly_rate", "cancellation_fee", "capacity"]).isNumeric(),
  body<gymForm>("auto_accept").isBoolean(),
  catchValidationErrors,
  async (req, res) => {
    const body = req.body as gymForm;
    const userId = getTokenPayload(res).user.id;

    const gym = new gymModel(<Gym>{
      title: body.title,
      capacity: body.capacity,
      address: body.address,
      owner: new ObjectId(userId),
      hourly_rate: body.hourly_rate,
      auto_accept: body.auto_accept,
      cancellation_fee: body.cancellation_fee,
      services: body.services,
      equipment: body.equipment,
      size: body.size
    });

    await gym.save();

    return res.status(201).json({ data: { id: gym.id } });
  }
];

export const getGym : RequestHandler[] = [
  async (req, res) => {
    const gymId = req.params.id;
    const gym = await gymModel.findById(gymId).exec();
    res.json({ data: gym });
  }
];

export const getGyms : RequestHandler[] = [
  async (req, res) => {
    const gyms = await gymModel.find({ public: true}).exec();
    res.json({ data: gyms });
  }
];

export const getMyGyms : RequestHandler[] = [
  async (req, res) => {
    const userId = getTokenPayload(res).user.id;
    const myGyms = await gymModel.find({ owner: userId }).exec();

    res.json({ data: myGyms });
  }
];

export const editGym : RequestHandler[] = [
  async (req, res) => {
    var token = getTokenPayload(res);
  }
];
