import express, { ErrorRequestHandler } from "express";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import routes from "./routes";
import config from "../config/config";
import expressValidator from "express-validator";
import cors from "cors";
import 'express-async-errors';

const PORT = config.PORT;
const app: express.Application = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(expressValidator());

var mongooseConnection = mongoose.connect(config.mongodb.connectionString, {
  useNewUrlParser: true,
  useCreateIndex: true
});

mongooseConnection.then(res => {
    routes(app);

    app.use(<ErrorRequestHandler>((err, req, res, next) => {
      res.status(500).json({ message: "Server error" });
      console.error(err);

      next(err);
    }));

    app.listen(PORT, () => {
        console.log(`Server listening on port ${PORT}...`)
    });
})
