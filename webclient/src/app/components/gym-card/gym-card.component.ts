import { Component, OnInit, Input } from '@angular/core';
import { Gym } from 'src/app/models/Gym';

@Component({
  selector: 'app-gym-card',
  templateUrl: './gym-card.component.html',
  styleUrls: ['./gym-card.component.scss']
})
export class GymCardComponent implements OnInit {

  @Input() gym : Gym;

  constructor() { }

  ngOnInit() {
  }

  get cardPicture() {
    return this.gym.pictures.length > 0
      ? this.gym.pictures[0]
      : "/assets/default_gym.jpg";
  }
}
