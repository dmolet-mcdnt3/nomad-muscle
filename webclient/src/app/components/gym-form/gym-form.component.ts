import { Component, OnInit, Input } from '@angular/core';
import { Gym } from 'src/app/models/Gym';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Address, formGroup } from 'src/app/models/Address';

interface GymFormModel {
  title: string;
  hourly_rate: number;
  cancellation_fee: number;
  capacity: number;
  address: Address,
  // services: string[];
  // equipment: string[];
  size?: { width: number, length: number };
  auto_accept: boolean;
}

@Component({
  selector: 'app-gym-form',
  templateUrl: './gym-form.component.html',
  styleUrls: ['./gym-form.component.scss']
})
export class GymFormComponent implements OnInit {

  form : FormGroup;

  @Input() gym : Gym;

  constructor(private fb : FormBuilder) {
  }

  buildForm(gym? : Gym) : FormGroup {
    return this.fb.group(<{ [key in keyof GymFormModel]}>{
      title: [ gym ? gym.title : "" ],
      description: [ gym ? gym.description : "" ],
      auto_accept: [gym ? gym.auto_accept : false],
      cancellation_fee: [gym ? gym.cancellation_fee : 0],
      capacity: [ gym ? gym.capacity : null ],
      hourly_rate: [],
      address: formGroup(gym ? gym.address : undefined),
      size: this.fb.group({
        width: [ gym && gym.size ? gym.size.width: null],
        length: [ gym && gym.size ? gym.size.length : null]
      })
    });
  }

  ngOnInit() {
    this.form = this.buildForm(this.gym);
  }

}
