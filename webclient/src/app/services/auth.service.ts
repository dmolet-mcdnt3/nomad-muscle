import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Address } from '../models/Address';

const AUTH_TOKEN_KEY = "token";
const jwtHelper = new JwtHelperService();

interface createUserBody {
  email: string;
  password: string;
  firstname: string;
  lastname: string;
  birthdate: string;
  address: Address;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private api : ApiService) { }

  login(email : string, password : string) : Observable<{}> {
    return this.api.post("user/login", {
      email: email,
      password: password
    }).pipe(
      tap((response : any) => {
        const token = response.data.token;
        this.setToken(token);
      })
    );
  }

  register(fields : createUserBody) {
    return this.api.post("user/register", fields);
  }

  signOut() : void {
    localStorage. removeItem(AUTH_TOKEN_KEY);
  }

  isLoggedIn() : boolean {
    const token = this.getToken();
    return token && !jwtHelper.isTokenExpired(token);
  }

  getUserInfo() :  any  {
      return jwtHelper.decodeToken(this.getToken());
  }

  setToken(token : string) : void {
    localStorage.setItem(AUTH_TOKEN_KEY, token);
  }

  getToken() : string {
    return localStorage.getItem(AUTH_TOKEN_KEY);
  }
}
