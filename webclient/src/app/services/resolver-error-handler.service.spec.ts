import { TestBed } from '@angular/core/testing';

import { ResolverErrorHandlerService } from './resolver-error-handler.service';

describe('ResolverErrorHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ResolverErrorHandlerService = TestBed.get(ResolverErrorHandlerService);
    expect(service).toBeTruthy();
  });
});
