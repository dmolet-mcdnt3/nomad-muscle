import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { map } from 'rxjs/operators';
import { Gym } from '../models/Gym';
import { Observable } from 'rxjs';
import { toQueryParamsStr } from '../utils';
import { Address } from 'cluster';

interface GymForm {
  title: string;
  hourly_rate: number;
  cancellation_fee: number;
  capacity: number;
  address: Address,
  services: string[];
  equipment: string[];
  size?: { width: number, length: number };
  auto_accept: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class GymsService {

  constructor(
    private api : ApiService
  ) { }

  getGym(id) : Observable<Gym>{
    return this.api.get("gyms/" + id).pipe(
      map((res : any) => res.data)
    );
  }

  getGyms(page? : number) : Observable<Gym[]> {
    const params : any = {};
    if (page) {
      params.p = page;
    }

    const queryString = toQueryParamsStr(params);
    return this.api.get("gyms" + queryString).pipe(
      map((res : any) => res.data)
    );
  }

  getMyGyms(page? : number) : Observable<Gym[]> {
    const params : any = {};
    if (page) {
      params.p = page;
    }

    const queryString = toQueryParamsStr(params);
    return this.api.get("gyms/mine" + queryString).pipe(
      map((res : any) => res.data)
    );
  }

  createGym(fields : GymForm) : Observable<string> {
    return this.api.post("gyms", fields).pipe(
      map((res : any) => res.data.id)
    );
  }
}
