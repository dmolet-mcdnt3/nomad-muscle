import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { AuthGuard } from './guards/auth.guard.service';
import { RegisterPageComponent } from './pages/register-page/register-page.component';
import { MyGymsPageComponent } from './pages/my-gyms-page/my-gyms-page.component';
import { GymResolver, GymsResolver, MyGymsResolver } from './resolvers/gym.resolver';
import { GymsPageComponent } from './pages/gyms-page/gyms-page.component';
import { GymPageComponent } from './pages/gym-page/gym-page.component';
import { NewGymPageComponent } from './pages/new-gym-page/new-gym-page.component';

const routes: Routes = [
  { path: 'home', component: HomePageComponent },
  { path: 'login', component: LoginPageComponent },
  { path: 'register', component: RegisterPageComponent },
  { path: 'gyms', children: [
      { path: '', pathMatch: 'full', resolve: { gyms: GymsResolver }, component: GymsPageComponent },
      { path: 'mine', resolve: { gyms: MyGymsResolver }, canActivate: [ AuthGuard ], component: MyGymsPageComponent },
      { path: 'new', canActivate: [ AuthGuard ], component: NewGymPageComponent },
      { path: ':id', resolve: { gym: GymResolver }, component: GymPageComponent }
    ]
  },
  { path: '', redirectTo: '/home', pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
