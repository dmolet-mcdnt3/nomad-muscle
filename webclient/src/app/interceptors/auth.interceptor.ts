import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from '../services/auth.service';
import { Observable, observable } from 'rxjs';
import { Config } from '../../config/config';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private auth : AuthService,
    private router : Router
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler) : Observable<HttpEvent<any>> {
    const token = this.auth.getToken();
    if (token && request.url.startsWith(Config.API_ROOT_URL)) {
        const newRequest = request.clone({
            setHeaders: {
                "Authorization": "Bearer " + token
            }
        });

        return next.handle(newRequest).pipe(
          tap(undefined, err => this.interceptError(err))
        );
    }

    return next.handle(request);
  }

  interceptError(error) {
    if (error instanceof HttpErrorResponse && error.status == 401) {
      this.auth.setToken(undefined);
      this.router.navigateByUrl("/login");
    }
  }
}
