import { RequestInfo, InMemoryDbService } from 'angular-in-memory-web-api';
import { Observable } from 'rxjs';

export class InMemoryDataService implements InMemoryDbService {
  createDb(reqInfo?: RequestInfo): {} | Observable<{}> | Promise<{}> {
    return {
      gyms: [],
      users: []
    };
  }
}
