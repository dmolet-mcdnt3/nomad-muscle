import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { httpInterceptorProviders } from './interceptors';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { RegisterPageComponent } from './pages/register-page/register-page.component';
import { FormsModule } from '@angular/forms';
import { MyGymsPageComponent } from './pages/my-gyms-page/my-gyms-page.component';
import { GymsPageComponent } from './pages/gyms-page/gyms-page.component';
import { GymPageComponent } from './pages/gym-page/gym-page.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { NewGymPageComponent } from './pages/new-gym-page/new-gym-page.component';
import { GymCardComponent } from './components/gym-card/gym-card.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { GymFormComponent } from './components/gym-form/gym-form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginPageComponent,
    HomePageComponent,
    RegisterPageComponent,
    MyGymsPageComponent,
    GymsPageComponent,
    GymPageComponent,
    NewGymPageComponent,
    GymCardComponent,
    GymFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [
    httpInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
