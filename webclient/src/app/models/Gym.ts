import { Address } from './Address';

export interface Gym {
  id: string;
  owner: string;
  title: string;
  description: string;
  hourly_rate: number;
  address: Address;
  services: string[];
  equipment: string[];
  size?: { width: number, length: number },
  pictures: string[];
  capacity: number;
  cancellation_fee: number;
  auto_accept: boolean;
  public: boolean;
}
