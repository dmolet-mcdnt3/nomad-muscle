import { FormGroup, FormBuilder } from '@angular/forms';

export interface Address {
  line1 : string
  line2 : string
  city : string
  postalCode : string,
};

const fb = new FormBuilder();

export function formGroup(v : Address) : FormGroup {
  return fb.group(<{ [key in keyof Address] }>{
    city: [v ? v.city : ""],
    line1:  [v ? v.line1 : ""],
    line2: [v ? v.line2 : ""],
    postalCode: [v ? v.postalCode : ""]
  });
}
