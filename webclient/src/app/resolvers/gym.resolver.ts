import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { GymsService } from '../services/gyms.service';
import { Gym } from '../models/Gym';

@Injectable({
  providedIn: 'root'
})
export class GymResolver implements Resolve<Gym> {
  constructor(
    private gymsService : GymsService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const gymId = route.params.id;
    return this.gymsService.getGym(gymId);
  }
}

@Injectable({
  providedIn: 'root'
})
export class GymsResolver implements Resolve<Gym[]> {
  constructor(
    private gymsService : GymsService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const page = route.queryParams.page;
    return this.gymsService.getGyms(page);
  }
}

@Injectable({
  providedIn: 'root'
})
export class MyGymsResolver implements Resolve<Gym[]> {
  constructor(
    private gymsService : GymsService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const page = route.queryParams.page;
    return this.gymsService.getMyGyms(page);
  }
}
