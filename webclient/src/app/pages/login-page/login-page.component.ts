import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  constructor(
    private authService : AuthService,
    private router : Router
  ) { }

  ngOnInit() {
    if (this.authService.isLoggedIn()) {
      this.router.navigateByUrl("/");
    }
  }

  onSubmit(form : NgForm) {
    const f = form.value;
    this.authService.login(f.email, f.password).subscribe(() => {
      this.router.navigateByUrl("/");
    }, (err) => alert(err.message));
  }
}
