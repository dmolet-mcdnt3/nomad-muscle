import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewGymPageComponent } from './new-gym-page.component';

describe('NewGymPageComponent', () => {
  let component: NewGymPageComponent;
  let fixture: ComponentFixture<NewGymPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewGymPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewGymPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
