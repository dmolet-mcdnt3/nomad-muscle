import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnInit {

  constructor(
    private authService : AuthService,
    private router : Router
  ) { }

  ngOnInit() {
    if (this.authService.isLoggedIn()) {
      this.router.navigateByUrl("/");
    }
  }

  onSubmit(form: NgForm) {
    const f = form.value;
    this.authService.register({
      email: f.email,
      password: f.password,
      firstname: f.firstname,
      lastname: f.lastname,
      birthdate: f.birthdate,
      address: {
        line1: f.line1,
        line2: f.line2,
        city: f.city,
        postalCode: f.postalCode
      }
    }).subscribe(() => {
      alert("Succès;");
      this.router.navigateByUrl("/login");
    }, (err) => alert(err.message));
  }
}
