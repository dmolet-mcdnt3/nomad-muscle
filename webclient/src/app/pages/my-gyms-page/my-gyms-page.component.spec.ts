import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyGymsComponent } from './my-gyms.component';

describe('MyGymComponent', () => {
  let component: MyGymsComponent;
  let fixture: ComponentFixture<MyGymsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyGymsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyGymsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
