import { Component, OnInit } from '@angular/core';
import { Gym } from 'src/app/models/Gym';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-gyms-page',
  templateUrl: './gyms-page.component.html',
  styleUrls: ['./gyms-page.component.scss']
})
export class GymsPageComponent implements OnInit {

  gyms : Gym[];

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.gyms = data.gyms;
    });
  }

}
