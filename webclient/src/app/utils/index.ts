// https://stackoverflow.com/a/34209399
/**
 * Generates a URL query string from an object
 */
export function toQueryParamsStr(params : {}) {
  function makeParamStr(key : string, value : string) {
    return encodeURIComponent(key) + '=' + encodeURIComponent(value);
  }

  const keys = Object.keys(params);
  if (keys.length == 0)
    return "";

  const str = keys.reduce((array, key) => {
    const value = params[key];
    if (value !== undefined) {
      if (Array.isArray(value)) {
        value.forEach(val => array.push(makeParamStr(key, val)));
      } else {
        array.push(makeParamStr(key, value));
      }
    }
    return array;
  }, [])
  .join('&');

  return str.length > 0
    ? "?" + str
    : "";
}
